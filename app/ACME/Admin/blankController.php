<?php

namespace App\Http\Controllers\Admin;

use App\ACME\Admin\AdminHelper;
use Illuminate\Http\Request;

class BlankController extends AdminHelper
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public $modal = 'BlankModal';
    public $viewSelector = 'admin.services.';

    public function index()
    {
        return $this->globalIndex($this->viewSelector, $this->modal);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return $this->globalCreate($this->viewSelector, $this->modal);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * if module has any type of file upload please add that file name (input type file name attr) after $id, in global update as a param
     */
    public function store(Request $request)
    {
        return $this->globalStore($request, $this->modal);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        return $this->globalEdit($id, $this->viewSelector, $this->modal);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     *  if module has any type of file upload please add that file name (input type file name attr) after $id, in global update as a param
     */
    public function update(Request $request, $id)
    {
        return $this->globalUpdate($request, $this->modal,$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return array
     */
    public function destroy($id)
    {
        return $this->globalDestroy($id, $this->modal);

    }

}
