<?php

namespace App\ACME;


use Illuminate\Http\Request;
use App\ACME\SingularToPlural;
use Illuminate\Support\Facades\Schema;
trait DBOperations {

    public $videoExtensions = ['mp4'];
    public $imgExtensions = ['jpeg','png','jpg','gif'];
    public $classPrefix = "App\Model\\";

    /**
     * @param Request $request
     * @param $fileKey //it should table field name as well as form input name
     * @param $primaryFolder
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function fileUploadExtension(Request $request, $fileKey, $primaryFolder)
    {
        $file = $request->file($fileKey);
        $fileExtension = $file->getClientOriginalExtension();
        $fileName = time() . '.' . $fileExtension;
        if (in_array($fileExtension, $this->imgExtensions)) {
            $destinationPrefix = '/image';
        } elseif (in_array($fileExtension, $this->videoExtensions)) {
            $destinationPrefix = '/video';
        } else {
            flash('File extension does not match with records')->warning();
            return redirect()->back()->withInput();
        }
        $destinationPath = public_path($primaryFolder . $destinationPrefix);
        $file->move($destinationPath, $fileName);
        $collection = array_merge($request->all(), [$fileKey => $fileName]);

        return $collection;
    }

    /**
     * @param $id
     * @param $modalName
     * @return array
     */
    public function globalDestroy($id, $modalName)
    {
        $modal = $this->classPrefix . $modalName;
        $findExistOrNot = $modal::find($id);
        $result = $findExistOrNot->delete();
        $information = [
            'type' => $result ? 1 : 0,
            'message' => $result ? $modalName . ' has been deleted' : $modalName . ' deletion failed',
            'trId' => $id,
        ];
        flash($modalName . ' deleted successfully!')->success();
        return response()->json($information);
    }
  
  /**
   * @param $id
   * @param $view
   * @param $modalName
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
   */
    public function globalEdit($id, $view, $modalName)
    {
        $modal = $this->classPrefix . $modalName;
        $details = $modal::find($id);
        if ($details) {
            return view($view . strtolower($modalName) . '.edit', compact('details'));
        } else {
            flash('Sorry! no ' . strtolower($modalName) . ' found')->warning();
            return redirect()->back();
        }
    }

    /**
     * @param Request $request
     * @param $modalName
     * @param string $hasFile
     * @return \Illuminate\Http\RedirectResponse
     */
    public function globalStore(Request $request, $modalName, $hasFile = '')
    {
        $modal = $this->classPrefix . $modalName;
        $validation = $modal::validate($request->all());

        if ($validation->fails()) {
            flash('Please fill all mandatory fields')->warning();
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $collection = $request->all();
        if ($hasFile != '') { //'video_file'
            $collection = $this->fileUploadExtension($request, $hasFile, '/' . strtolower($modalName));
        }

        $modal::create($collection);

        flash(strtolower($modalName) . ' created successfully!')->success();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @param $modalName
     * @param $id
     * @param string $hasFile
     * @return \Illuminate\Http\RedirectResponse
     */
    public function globalUpdate(Request $request, $modalName, $id, $hasFile = '')
    {
        $modal = $this->classPrefix . $modalName;

        $validation = $modal::validate($request->all(), $id);

        if ($validation->fails()) {
            flash('oOps!Something went wrong')->warning();
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }

        $collection = $request->all();
        if ($hasFile != '') {
            $collection = $this->fileUploadExtension($request, $hasFile, '/' . strtolower($modalName));
        }

        $modal::find($id)->fill($collection)->save();

        flash($modalName . ' updated successfully!')->success();

        return redirect()->back();
    }
  
  /**
   * @param $view
   * @param $modalName
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
    public function globalIndex($view, $modalName)
    {
        $obj = new SingularToPlural();
        $tableName = $obj->underscore($obj->pluralize($modalName));
        if(!Schema::hasTable($tableName)) {
            flash($tableName . ' table not found in database')->success();
            flash('Please do db migrate (php artisan migrate)')->success();
            return redirect()->back();
        }
        $modal = $this->classPrefix . $modalName;
        $collection = $modal::orderBy('id', 'desc')->get();
        return view($view . strtolower($modalName) . '.grid', compact('collection'));
    }
  
  /**
   * @param $view
   * @param $modalName
   * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
   */
    public function globalCreate($view, $modalName)
    {
        return view($view . strtolower($modalName) . '.create');
    }
}