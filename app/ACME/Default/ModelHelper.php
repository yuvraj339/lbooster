<?php
namespace App\ACME\Model;

use Illuminate\Database\Eloquent\Model;
use Validator;

class ModelHelper extends Model
{
  /**
   * @param $requestedData
   * @param null $id
   * @return mixed
   * @internal param $data
   */
  public static function validation($requestedData, $rule)
  {
    return Validator::make($requestedData, $rule);
  }
}