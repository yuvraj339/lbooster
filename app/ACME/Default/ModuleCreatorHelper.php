<?php
/**
 * Created by PhpStorm.
 * User: Yuvraj
 * Date: 8/10/2019
 * Time: 5:13 PM
 */

namespace App\ACME;


use App\ACME\Admin\AdminHelper;
use App\Model\ModuleCreator;
use Artisan;
use DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Schema;
use App\ACME\SingularToPlural;



class ModuleCreatorHelper extends AdminHelper
{

    /**
     * ModuleCreatorHelper constructor.
     */
    public function __construct()
    {
        $this->singularToPlural = new SingularToPlural();
        if (PHP_OS == 'WINNT') {
            $this->delFile = 'del ';
        } else {
            $this->delFile = 'rm -rf ';
        }
        parent::__construct();
    }

    protected $singularToPlural;
    protected $viewName = 'admin.blank';
    protected $validationRules = "\n\t\t\t\t\t" . "'my_name'   => 'honeypot',\n\t\t\t\t\t'my_time'   => 'required|honeytime:5',";
    protected $validateSection = "\n\t" . '/**' . "\n\t" . ' * @param $requestedData' . "\n\t" . ' * @param null $id' . "\n\t" . ' * @return mixed' . "\n\t" . ' * @internal param $data' . "\n\t" . ' */ ' . "\n\t" . ' public static function validate($requestedData, $id = null) { ' . "\n\t\t" . ' return static::validation($requestedData, static::rules($id)); ' . "\n\t" . ' }';
    protected $formFields = "";
    protected $delFile;
    protected $ruleSection = "\n\t" . '/**' . "\n\t" . ' * Validation Rules' . "\n\t" . ' * @param null $id' . "\n\t" . ' * @return array ' . "\n\t" . ' */' . "\n\t" . 'public static function rules($id = null) {' . "\n\t\t" . 'if ($id) {' . "\n\t\t\t" . 'return [validationRuleHere' . "\n\t\t\t" . '];' . "\n\t\t" . ' }' . "\n\t\t" . ' return [validationRuleHere' . "\n\t\t\t" . '];' . "\n\t" . ' }';
    protected $viewDestinationPath = '';
    protected $fieldsName = "\t" . 'protected $fillable = [' . "\n\t\t";

    /**
     * @param Request $request
     */
    public function createViews(Request $request)
    {
        $controllerRelated = strtolower($request->controller_related_to);
        $source = resource_path("views" . $this->separators . $controllerRelated . $this->separators . "blank");
        $relatedModule = $request->related_module != '' ? $this->separators . strtolower($request->related_module) : '';

        $groupDir = "mkdir " . resource_path("views" . $this->separators . $controllerRelated . $relatedModule);
        exec($groupDir);

        $this->viewDestinationPath = resource_path("views" . $this->separators . $controllerRelated . $relatedModule . $this->separators . strtolower($request->module_name));
        $viewDir = "mkdir " . $this->viewDestinationPath;
        exec($viewDir);

        $this->copy_directory($source, $this->viewDestinationPath);
        flash('Views created for ' . $request->module_name)->success();
    }

    /**
     * @param $src
     * @param $dst
     */
    function copy_directory($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src . '/' . $file)) {
                    recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
     * @param $status
     * @param $uid
     * @return RedirectResponse
     */
    public function updateModuleStatus($status, $uid)
    {
        $checkModuleExistence = ModuleCreator::find($uid);
        if ($checkModuleExistence == null) {
            flash('oOps!Something went wrong')->warning();
            return redirect()->back();
        }

        if ($status) {
            $status = 0;
            flash('Module deactivated successfully!')->success();
        } else {
            $status = 1;
            flash('Module activated successfully!')->success();
        }

        $checkModuleExistence->update(['active' => $status]);

        return redirect()->back();
    }

    /**
     * @param $fileName
     * @param $appendData
     * @param string $extension
     * @param string $matchWith
     */
    public function dataAppendScript($fileName, $appendData, $extension = '.php', $matchWith = '')
    {
        $file = $fileName . $extension;

        // read lines with fgets() until you have reached the right one
        //insert the line and than write in the file.
        if ($matchWith == '') {
            file_put_contents($file, $appendData, FILE_APPEND);

        } else {
            $f = fopen($file, "r+");
            $fileAvailableText = file_get_contents($file);

            while (($buffer = fgets($f)) !== false) {
                if (strpos($buffer, $matchWith) !== false) {
                    $pos = ftell($f);
                    $newstr = substr_replace($fileAvailableText, $appendData, $pos, 0);
                    file_put_contents($file, $newstr);
                    break;
                }
            }
            fclose($f);
        }

    }

    /**
     * @param $controllerRelated
     * @param $fileName
     * @param $extension
     */
    public function addResourceController($controllerRelated, $fileName, $extension, $relatedModule)
    {
        $source = app_path("ACME" . $this->separators . $controllerRelated . $this->separators . "blankController.php");
        $controllerName = ucwords($fileName) . 'Controller';
        $modalName = ucwords($fileName);
        $this->viewName = strtolower($controllerRelated) . '.' . strtolower($relatedModule) . '.';
        $destination = app_path("Http" . $this->separators . "Controllers" . $this->separators . $controllerRelated . $this->separators . $controllerName . $extension);
        copy($source, $destination);
        $f = fopen($destination, "r+");
        $fileAvailableText = file_get_contents($destination);

        $updateClassName = str_replace('BlankController', $controllerName, $fileAvailableText);
        $updateModalName = str_replace('BlankModal', $modalName, $updateClassName);
        $updateView = str_replace('admin.services.', $this->viewName, $updateModalName);
        file_put_contents($destination, $updateView);

        fclose($f);
    }

    /**
     * @param $modelName
     */
    public function modelUpdate($modelName)
    {
        $modelName .= '.php';
        $source = app_path("Model" . $this->separators . $modelName);
        $f = fopen($source, "r+");
        $fileAvailableText = file_get_contents($source);
        $updateClassName = str_replace('use Illuminate\Database\Eloquent\Model;', 'use App\ACME\Model\ModelHelper;', $fileAvailableText);
        $updateExtend = str_replace('extends Model', 'extends ModelHelper', $updateClassName);
        $updateValidationRules = str_replace('validationRuleHere', $this->validationRules, $updateExtend);
        file_put_contents($source, $updateValidationRules);
        fclose($f);
    }


    /**
     * @param $fileName
     * @param string $replace
     * @param string $extension
     * @param string $matchWith
     */
    public function updateFile($fileName, $matchWith = '', $replace = '', $extension = '.php')
    {
        $fileName .= $extension;
        $f = fopen($fileName, "r+");
        $fileAvailableText = file_get_contents($fileName);
        $updatedFile = str_replace($matchWith, $replace, $fileAvailableText);
        file_put_contents($fileName, $updatedFile);
        fclose($f);
    }
    /**
     * @param $field
     */
    public function makeValidation($field)
    {
        $validationType = $field->data_type;
        $max = '|max:191';
        if ($validationType == 'integer') {
            $validationType = 'numeric';
            $max = '|max:12';
        } else if ($validationType == 'text') {
            $max = '|max:255';
        }

        $isRequire = $field->is_required ? 'required|' : '';

        $inputType = $validationType . $max;
        $this->validationRules .= "\n\t\t\t\t\t'" . $field->name . "' => '" . $isRequire . $inputType . "',";
    }

    /**
     * @param $field
     * @return string
     */
    public function createFormFields($field)
    {
        $inputType = $field->data_type == 'text' ? 'textarea' : 'text';
        $rows = $inputType == 'text' ? '' : ",'rows' => 4";
        $formField = '<div class="col-lg-6">';
        $formField .= "\n\t" . '<div class="form-group';

        /*if ($field->is_required) {*/

        $formField .= '{{ $errors->has(\'' . $field->name . '\') ? \' has-error\' : \'\' }}';
        //}

        $formField .= '">';
        $formField .= "\n\t\t" . '{!! Form::label(\'' . $field->name . '\', \'' . $field->name . '\', [\'class\' => \'control-label\']) !!}';

        if ($field->is_required) {
            $formField .= "\n\t\t" . '<span class="text-danger">*</span>';
        }

        $formField .= "\n\n\t\t" . '{!! Form::' . $inputType . '(\'' . $field->name . '\', null, [\'class\' => \'form-control\', \'placeholder\' => \'Enter ' . $field->name . '\'' . $rows . ']) !!}';

        /*if ($field->is_required) {*/
        $formField .= "\n\t\t" . '@if($errors->has(\'' . $field->name . '\'))';
        $formField .= "\n\t\t\t" . '<span class="text-danger" >  {{ $errors->first(\'' . $field->name . '\') }}   </span>';
        $formField .= "\n\t\t" . '@endif';
        //}

        $formField .= "\n\t" . '</div>' . "\n" . '</div>' . "\n";
        return $formField;
    }

    /**
     * @param $destination
     * @param $modelName
     */
    public function updateView($modelName)
    {
        $viewPath = $this->viewDestinationPath;
        $views = [$this->separators . 'create.blade.php', $this->separators . 'create-edit-common.blade.php', $this->separators . 'edit.blade.php', $this->separators . 'grid.blade.php'];
        $viewName = $this->viewName . strtolower($modelName);
        foreach ($views as $view) {
            $destination = $viewPath . $view;
            $f = fopen($destination, "r+");
            $fileAvailableText = file_get_contents($destination);

            $viewUpdate = str_replace('admin.blank', $viewName, $fileAvailableText);
            $updateClassName = str_replace('Blank', ucwords($modelName), $viewUpdate);
            $updateModalName = str_replace('blank', strtolower($modelName), $updateClassName);
            $fields = str_replace('{{--fields--}}', $this->formFields, $updateModalName);
            file_put_contents($destination, $fields);
            fclose($f);
        }

    }

    /**
     * @param Request $request
     * @param $makeModelFile
     * @param $createViewFolder
     * @return array
     */
    protected function createMVC(Request $request, $makeModelFile)
    {
        $migrationName = $this->singularToPlural->underscore($this->singularToPlural->pluralize($request->module_name));
        $allMigrationFields = json_decode($request->allMigrationFields);

        list($makeMigration, $makeMigrationCommand) = $this->makeMigrationWithSchema($request, $migrationName, $allMigrationFields);
        flash($makeModelFile)->success();

        if (isset($request->migrate_now) && !Schema::hasTable($migrationName)) {
            Artisan::call('migrate');
            $migrate = Artisan::output();
            flash($migrate)->success();
        } //can set flash msg for showing table already exist
        $this->updateView($request->module_name);
        return [$makeMigration, $makeMigrationCommand];
    }

    /**
     * @param Request $request
     * @param $migrationName
     * @param $allMigrationFields
     * @return mixed|string
     */
    protected function makeMigrationWithSchema(Request $request, $migrationName, $allMigrationFields)
    {
        //make migration with schema
        $makeMigration = '';
        $makeMigrationCommand = '';
        if (isset($request->make_migration)) {

            $makeMigrationCommand = 'php ' . base_path() . $this->separators . 'artisan make:migration:schema create_' . $migrationName . '_table --model=0 --schema="';
            $makeMigrationCommand = $this->getAllMigrationFields($allMigrationFields, $makeMigrationCommand);
//            $makeMigrationCommand = str_replace_last(',', '"', $makeMigrationCommand);
            $makeMigrationCommand .= '"';
            exec($makeMigrationCommand);

            //ToDO : change find command for windows (without change make migration field empty and not delete when module delete)
            $getLatestMigrationName = "find " . database_path('migrations') . " -name '*create_" . $migrationName . "_table.php*'";
            $files = exec($getLatestMigrationName);
            $makeMigration = last(explode('/', $files));

            $removeUnwantedModel = $this->delFile . app_path() . $this->separators . ucwords($request->module_name);
            exec($removeUnwantedModel);

            flash('Migration created successfully : ' . $makeMigration)->success();
        }
        return [$makeMigration, $makeMigrationCommand];
    }

    /**
     * @param $allMigrationFields
     * @param $makeMigrationCommand
     * @return string
     */
    protected function getAllMigrationFields($allMigrationFields, $makeMigrationCommand)
    {
        foreach ($allMigrationFields as $migrationField) {

            $this->formFields .= $this->createFormFields($migrationField);
            $this->validationRules .= $this->makeValidation($migrationField);

            $makeMigrationCommand .= $migrationField->name . ':' . $migrationField->data_type;
            if ($migrationField->data_type == 'boolean') {
                $makeMigrationCommand .= '(0)';
            }
            $this->fieldsName .= "'" . $migrationField->name . "',";
            if ($migrationField->default_check) {
                if ($migrationField->default == '') {
                    $migrationField->default = '0';
                }

                $makeMigrationCommand .= ':default(' . $migrationField->default . ')';
            }

            if ($migrationField->nullable) {
                $makeMigrationCommand .= ':nullable';
            }

            if ($migrationField->unique) {
                $makeMigrationCommand .= ':unique';
            }
            $makeMigrationCommand .= ',';
        }
        $this->fieldsName .= "'status'\n\t];" . $this->validateSection . $this->ruleSection;
        $makeMigrationCommand .= 'status:boolean:default(0)';
        return $makeMigrationCommand;
    }

    /**
     * @param $module
     * @return array
     */
    protected function getAllPathForModuleDeletion($module)
    {
        //get controller path and check controller related to admin or frontend
        if ($module->controller_related_to == 'Admin') {
            $controllerPath = $this->delFile . app_path() . $this->separators . "Http" . $this->separators . "Controllers" . $this->separators . $module->controller_related_to . $this->separators . ucwords($module->module_name) . 'Controller.php';

            if (PHP_OS == 'WINNT') {
                $viewPath = ' rmdir /s ' . app()->resourcePath() . $this->separators . "views" . $this->separators . strtolower($module->controller_related_to) . $this->separators . strtolower($module->related_module) .  $this->separators .  strtolower($module->module_name) . ' /q';
            } else {
                $viewPath = $this->delFile . app()->resourcePath() . $this->separators . "views" . $this->separators . strtolower($module->controller_related_to) . $this->separators . strtolower($module->related_module) . $this->separators . strtolower($module->module_name);
            }

        } else {

            $controllerPath = $this->delFile . app_path() . $this->separators . "Http" . $this->separators . "Controllers" . $this->separators . "Frontend" . ucwords($module->module_name);

            $viewPath = "";
        }

        $this->dropDBTableIfExist($module);

        //get migration path
        $migrationPath = '';
        if ($module->make_migration != 'no' && $module->make_migration != '') {
            $migrationName = $module->make_migration;
            $migrationPath = $this->delFile . app()->databasePath() . $this->separators . "migrations" . $this->separators . $migrationName;
            return array($controllerPath, $viewPath, $migrationPath);
        }
        return array($controllerPath, $viewPath, $migrationPath);
    }

    /**
     * @param $module
     * @return string
     */
    protected function dropDBTableIfExist($module)
    {
        $migrationName = $this->singularToPlural->underscore($this->singularToPlural->pluralize($module->module_name));
//        $tables = DB::select('SHOW TABLES');
//        foreach ($tables as $table) {
//            foreach ($table as $key => $value) {
        if (Schema::hasTable($migrationName) == 1) {

            DB::beginTransaction();
            //turn off referential integrity
            DB::statement("DROP TABLE $migrationName");
            //turn referential integrity back on
            DB::commit();
        }
//            }
//        }
        return $migrationName;
    }
}