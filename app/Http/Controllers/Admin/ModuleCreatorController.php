<?php

namespace App\Http\Controllers\Admin;

use App\ACME\ModuleCreatorHelper;
use App\Model\ModuleCreator;
use App\Model\Permission;
use Artisan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ModuleCreatorController extends ModuleCreatorHelper
{

    /**
     * ModuleCreatorController constructor.
     */
    public function __construct()
    {
        ini_set('max_execution_time', 300);
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $modules = ModuleCreator::all();
        return view('admin.module.grid', compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.module.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validation = ModuleCreator::validate($request->all());
        if ($validation->fails()) {
            flash('Please fill all mandatory fields')->warning();
            return redirect()->back()->withInput()->withErrors($validation->errors());
        }
        $fileName = "Model" . $this->separators . ucwords($request->module_name);
        Artisan::call('make:model', ['name' => $fileName]);
        $makeModelFile = Artisan::output();

        $this->addResourceController($request->controller_related_to, $request->module_name, '.php', $request->related_module);
        flash('Controller created successfully')->success();

        //view creation
        $this->createViews($request);
        list($makeMigration, $makeMigrationCommand) = $this->createMVC($request, $makeModelFile);
        $this->dataAppendScript(app_path($fileName), $this->fieldsName, '.php', '{');
        $this->modelUpdate($request->module_name);

        //update module information in database
        ModuleCreator::createModule($request, $makeMigration, $makeMigrationCommand);

        //append route
        $matchWith = "Route::group(['namespace' => 'Admin'], function () {";
        $controllerName = ucwords($request->module_name) . 'Controller';
        $appendData = "\n\t\tRoute::resource('" . strtolower($request->module_name) . "', '" . $controllerName . "');" . "\n";
        $this->dataAppendScript(base_path('routes' . $this->separators . 'web'), $appendData, '.php', $matchWith);

        //append permission into csv and db
        $permissionName = strtolower($request->module_name);
        $permissions = Permission::getModulePermissionSchema($permissionName, $request->related_module);
        $permissionStr = '';
//        $count = Permission::count();
        foreach ($permissions as $permission) {
            $permissionStr .= $permission['name'] . ',' . $permission['display_name'] . ',' . $permission['description'] . ',' . $permission['related_module'] . ',' . $permission['auto_select'] . ',,' . "\n";
        }
        Permission::insertPermission($permissions);
        flash('Permissions added successfully!')->success();
        // "seeds\csvs\permissions";
        $seederPath = "seeds" . $this->separators . "csvs" . $this->separators . "permissions";
        $this->dataAppendScript(database_path($seederPath), $permissionStr, '.csv');

        return redirect()->back()->with(['makeNewModule' => 'Controller, Model, Migration, View, Permissions etc has been created successfully'], 'default');

    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $module = ModuleCreator::find($id);
        if ($module) {

            $modelPath = $this->delFile . app_path() . $this->separators . "Model" . $this->separators . $module->module_name . ".php";
            list($controllerPath, $viewPath, $migrationPath) = $this->getAllPathForModuleDeletion($module);
            exec($controllerPath);
            exec($viewPath);
            if ($migrationPath != '') {
                exec($migrationPath);
            }
            exec($modelPath);

            //remove permissions
            Permission::removeModulePermission(strtolower($module->module_name));

            //remove permission from permission csv
            $permissions = Permission::getModulePermissionSchema(strtolower($module->module_name), $module->related_module);
            foreach ($permissions as $permission) {
                $permissionStr = $permission['name'] . ',' . $permission['display_name'] . ',' . $permission['description'] . ',' . $permission['related_module'] . ',' . $permission['auto_select'] . ',,' . "\n";
                $seederPath = "seeds" . $this->separators . "csvs" . $this->separators . "permissions";
                $this->updateFile(database_path($seederPath), $permissionStr, '', '.csv');
            }
            // remove routes
            $controllerName = ucwords($module->module_name) . 'Controller';
            $matchWith = "\n\t\t"."Route::resource('" . strtolower($module->module_name) . "', '" . $controllerName . "');";
            $this->updateFile(base_path('routes' . $this->separators . 'web'), $matchWith);

            $result = $module->delete();
            $alert = [
                'type' => $result ? 1 : 0,
                'message' => $result ? 'Model, Controller, View, and Migration has been deleted successfully' : 'Module deletion failed',
            ];
            flash('Module deleted successfully!')->success();

            return response()->json($alert);
        } else {
            flash('oOps!Something went wrong')->warning();
            return redirect()->back();
        }
    }


}
