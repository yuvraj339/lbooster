<?php

namespace App\Http\Controllers\Auth;

use App\ACME\Frontend\FrontendHelper;
use App\ACME\Helper;
use App\Model\User;
use App\Model\Permission;
use App\Model\Role;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Auth;
use Route;
use Illuminate\Support\Str;

class LoginController extends FrontendHelper
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    protected $loginPath = '/';
    protected $afterLoginRedirection = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
        $this->middleware('guest')->except('logout');
        $this->guard()->logout();
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */


    public function login(Request $request)
    {
        $this->validateLogin($request);

        $userActive = User::getUserValue('email', $request->email, 'active');
        //check captcha
        /*if (env('APP_URL') != 'http://localhost') {
            $googleCaptchaValidation = $this->captchaRules($request->get('g-recaptcha-response'));
            if (!$googleCaptchaValidation) {
                return redirect('login')->withInput()->withErrors(['userActivation' => 'Oops something went wrong on google re  captcha.'], 'default');
            }
        }*/
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if ($userActive) {
                $this->sendLoginResponse($request);
                return redirect($this->afterLoginRedirection);
            } else
                return redirect()->back()->withInput()->withErrors(['userActivation' => 'User not activated yet please contact to admin'], 'default');

        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function authenticated(Request $request, $user)
    {
        if ($user) {
            if($user->role == null) {
                $this->logout($request);
                Session::flash('error', 'You have no role assign please contact to admin.');
                $this->afterLoginRedirection =  '/';
            } else {

                $rolePerms = $this->setRolePermissionsInSession();
                if(empty($rolePerms)) {
                    $this->logout($request);
                    Session::flash('error', 'Your assigned role dose not have any permission please contact to admin.');
                    $this->afterLoginRedirection =  '/';
                    return redirect('/');
                }

                $route = $user->role->roleInfo;

                if ($route &&  $route->page != null) {
                        $this->afterLoginRedirection =  $route->page;
                } else {
                    $this->afterLoginRedirection =  'admin';
                }
            }

        }
    }

    /**
     * @return array
     */
    protected function setRolePermissionsInSession()
    {
        session(['new_login' => 1]);
        if (!Session::has('rolePerms')) {
            $userType = Helper::getRoleNameById(Auth::user()->role->role_id);
            if ($userType == 'administration') {
                $rolePerms = Permission::pluck('name', 'id')->toArray();
                session(['rolePerms' => $rolePerms]);
                session(['uuid' => (string) Str::uuid()]);
                return $rolePerms;
            } else {
                $rolePerms = array_change_key_case( Role::rolePerms(Auth::user()->role->role_id), CASE_LOWER);
                session(['rolePerms' => $rolePerms]);
                session(['uuid' => (string) Str::uuid()]);
                return $rolePerms;
            }
        } else {
            $rolePerms = Session::get('rolePerms');
            return $rolePerms;
        }
    }

}