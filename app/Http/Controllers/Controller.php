<?php

namespace App\Http\Controllers;

use View;
use Route;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $serialNumberCount = 1;
    public $userPermissions = [];

    public function __construct()
    {
        View::share ( 'serialNumberCount', $this->serialNumberCount );
    }

}
