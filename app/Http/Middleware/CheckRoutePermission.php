<?php

namespace App\Http\Middleware;

use URL;
use Auth;
use Route;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Session;

class CheckRoutePermission
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $previousUrl = URL::previous();
        if (Auth::user() != null) {
            /*
             * check is session permission is there or not call below function only when permissions are deleted
             */
            if (Auth::user()->role != null) {
                $rolePerms = Session::get('rolePerms');
                // echo '<pre />'; print_r($rolePerms); die;
                if (!Session::has('defaultRoute')) {
                    session(['defaultRoute' => Route::currentRouteName()]);
                }
                if ($this->auth->guest()) {
                    if ($request->ajax()) {
                        return response()->json(
                            [
                                'status' => 0,
                                'code' => 401,
                                'message' => 'No permission to operate'
                            ]
                        );
                    } else {
                        return redirect()->guest('auth/login');
                    }
                }
                if (is_array($rolePerms) && !in_array(strtolower(Route::currentRouteName()), $rolePerms)) {
                    if ($request->ajax()) {
                        return response()->json(
                            [
                                'status' => 0,
                                'code' => 401,
                                'message' => 'No permission to operate'
                            ]
                        );
                    } else {
                        return redirect('admin/errors/401')->with('previousUrl', $previousUrl);
                    }
                }
            } else {
                return redirect('admin/errors/401')->with('previousUrl',$previousUrl);
            }
        }
        return $next($request);
    }
}
