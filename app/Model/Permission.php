<?php

namespace App\Model;

use Auth;
use Session;
use Validator;
use App\ACME\Helper;
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    protected $fillable = [
        'name', 'display_name', 'description', 'related_module', 'auto_select'
    ];

    /**
     * @param $data
     * @param null $id
     * @return mixed
     */
    public static function validate($data, $id = null)
    {
        return Validator::make($data, static::rules($id));
    }

    /**
     * Validation Rules
     * @param null $id
     * @return array
     */
    public static function rules($id = null)
    {
        if ($id) {
            return [
                'name' => 'required|string|max:191|unique:permissions,name,' . $id,
                'display_name' => 'max:191',
                'description' => 'max:191|',
                'related_module' => 'max:191|required|string',
                'my_name'   => 'honeypot',
                'my_time'   => 'required|honeytime:5'
            ];
        }

        return [
            'name' => 'required|string|max:191|unique:permissions',
            'display_name' => 'max:191',
            'description' => 'max:191|',
            'related_module' => 'max:191|required|string',
            'my_name'   => 'honeypot',
            'my_time'   => 'required|honeytime:5'
        ];
    }

    /**
     * @param $roleDetails
     * @param $id
     */
    public static function updatePermission($permissionDetails, $id)
    {
        $userIdForUpdate = static::find($id);
        $userIdForUpdate->fill($permissionDetails)->save();
    }

    /**
     * @param $permissionName
     * @return array
     */
    public static function getModulePermissionSchema($permissionName, $relatedModule)
    {
        return $permissions = array(
            array('name'=>$permissionName.'.index', 'display_name'=> $permissionName . ' index', 'description'=> $permissionName . ' index', 'related_module' => $relatedModule, 'auto_select' => 0),
            array('name'=>$permissionName.'.edit', 'display_name'=> $permissionName . ' edit', 'description'=> $permissionName . ' edit', 'related_module' => $relatedModule, 'auto_select' => 0),
            array('name'=>$permissionName.'.show', 'display_name'=> $permissionName . ' show', 'description'=> $permissionName . ' show', 'related_module' => $relatedModule, 'auto_select' => 0),
            array('name'=>$permissionName.'.update', 'display_name'=> $permissionName . ' update', 'description'=> $permissionName . ' update', 'related_module' => $relatedModule, 'auto_select' => 0),
            array('name'=>$permissionName.'.destroy', 'display_name'=> $permissionName . ' destroy', 'description'=> $permissionName . ' destroy', 'related_module' => $relatedModule, 'auto_select' => 0),
            array('name'=>$permissionName.'.create', 'display_name'=> $permissionName . ' create', 'description'=> $permissionName . ' create', 'related_module' => $relatedModule, 'auto_select' => 0),
            array('name'=>$permissionName.'.store', 'display_name'=> $permissionName . ' store', 'description'=> $permissionName . ' store', 'related_module' => $relatedModule, 'auto_select' => 0),
        );
    }

    /**
     * @param $permissions
     * @return mixed
     */
    public static function insertPermission($permissions)
    {
        $permissions = static::insert($permissions); // Eloquent approach
        $userType = Helper::getRoleNameById(Auth::user()->role->role_id);
        if ($userType == 'administration') {
            $rolePerms = static::pluck('name', 'id')->toArray();
            session(['rolePerms' => $rolePerms]);
            return $rolePerms;
        }
        return $permissions;
    }

    /**
     * @param $permissionName
     * @return bool
     */
    public static function removeModulePermission($permissionName)
    {
        static::where('name',$permissionName.'.index')
            ->orWhere('name',$permissionName.'.edit')
            ->orWhere('name',$permissionName.'.show')
            ->orWhere('name',$permissionName.'.update')
            ->orWhere('name',$permissionName.'.destroy')
            ->orWhere('name',$permissionName.'.create')
            ->orWhere('name',$permissionName.'.store')
            ->delete();
    }
}