<?php

namespace App\Providers;

use App\ACME\Helper;
use Auth;
use App\Model\Role;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(['layouts.admin.partial.admin-left-menus'], function ($view) {
            $rolePerms = Session::has('rolePerms') && is_array(Session::get('rolePerms')) ? Session::get('rolePerms') : [];
            $authRoleName = Auth::user()->role != null ? Helper::getRoleNameById(Auth::user()->role->role_id) : 'Not Defined';

            $view->with(['authRoleName'=> $authRoleName, 'rolePerms'=> $rolePerms]);
        });
    }
}
