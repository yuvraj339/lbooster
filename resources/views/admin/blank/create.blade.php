@extends('layouts.admin.layout')

@section('title')
    Add Blank
@stop
@section('content')

    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Blank','levelOneLink'=>url('admin/blank'),'levelTwo'=>'Create','levelTwoLink'=>null])

    {{--create sloat and componet for code optimizatrion--}}
    @component('layouts.admin.partial.panel')
    @slot('panelTitle', 'Add Blank')
    @slot('panelBody')

        {!! Form::open(['url'=> 'admin/blank', 'class' => ''] ) !!}

             @include('admin.blank.create-edit-common',['submitButtonName'=>'save'])

        {!! Form::close() !!}

    @endslot
    @endcomponent


@endsection
