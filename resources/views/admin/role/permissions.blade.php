@extends('layouts.admin.layout')
@section('title')
    Add competence
@stop
@section('css')
    <style lang="css">

        .modal-block-heading {
            font-weight: 300;
            margin-top: 0;
            font-size: 18px;
            padding: 0 0 10px;
            border-bottom: 1px solid #ddd;
            overflow: hidden;
            clear: both;
            display: inline-block;
        }

        .access-block-inner {
            background: #fff;
            margin: 0 0 15px;
            /*box-shadow: 2px 5px 7px #ddd;*/
            position: relative;
            /*min-height: 150px;*/
        }

        .access-block-inner > input {
            left: 5px;
            margin: 0 auto;
            position: absolute;
            top: 14px;
            /*transform: translateY(-50%);*/
        }

        .access-block-body {
            display: none;
        }

        .access-block-body.in {
            display: block;
        }

        #select-all.inline {
            display: inline-block;
            vertical-align: top;
        }
        .general-detail-page {
            position: relative;
        }

        .select-all-permission > input {
            margin: 4px;
        }

        .disabled-dispatch-screen .lead {
            color: #333;
        }

        .access-block-inner .access_heading::after {
            color: #000000;
            content: "+";
            /*float: right;*/
            font-size: 20px;
            font-weight: bold;
            line-height: 0;
            margin-left: 5px;
            position: absolute;
            right: 10px;
            top: 22px;
        }

        .access-block-inner .access_heading:hover::after {
            color: #fff;
        }

        .access-block-inner .minus-togle-sign.access_heading::after {
            content: "−";
        }

        .permission-accordion > .col-sm-6:nth-child(2n) {
            float: right;
        }

        .access_heading.btn.btn-info:hover, .access_heading.btn.btn-info:active, .access_heading.btn.btn-info:focus {
            background-color: #50a721;
            border-color: #50a721;
            opacity: 1;
            color: #fff !important;
        }

        .access_heading.btn.btn-info.minus-togle-sign {
            background-color: #50a721;
            border-color: #50a721;
            opacity: 1;
            color: #fff !important;
        }

        .access-block-inner .access_heading.btn.btn-info.minus-togle-sign::after {
            color: #fff;
        }

        .access_heading {
            padding: 10px 30px;
            background-color: #dcdcdc;
            border-color: rgba(167, 169, 171, 0.39);
            font-size: 14px;
            color: #3d3f43 !important;
            font-weight: normal;
            margin: 0;
            display: block;
            text-align: left;
        }

        h3.access_heading {
            font-family: monospace;
        }

        .staff-block .nav.side-menu-list li {
            padding: 5px 0;
            overflow: hidden;
            line-height: 1px;
        }

        .permission-chck::after {
            background: #ffffff none repeat scroll 0 0;
            border: 1px solid #a5a3a3;
            border-radius: 4px;
            content: " ";
            height: 15px;
            margin-top: -4px;
            pointer-events: none;
            position: absolute;
            width: 15px;
            left: 4px;
            z-index: 999;
        }

        input.permission-chck[type="checkbox"] {
            left: 5px;
            opacity: 0;
            position: absolute;
            z-index: 9;
            top: 10px;
        }

        input.permission-chck[type="checkbox"]:checked + .permission-chck::before {
            color: #44cc44;
            content: "";
            font-family: "FontAwesome";
            font-size: 14px;
            margin-left: 5px;
            margin-top: -7px;
            pointer-events: none;
            position: absolute;
            z-index: 1000;
        }

        label.permission-chck {
            display: inline-block;
            left: 0;
            margin-right: 28px;
            position: relative;
            top: 7px;
        }

        .permission-chck-child::after {
            background: #ffffff none repeat scroll 0 0;
            border: 1px solid #a5a3a3;
            border-radius: 4px;
            content: " ";
            height: 20px;
            margin-top: -5px;
            pointer-events: none;
            position: absolute;
            width: 20px;
            left: 2px;
            z-index: 999;
        }

        #permission-checked .permission-accordion-checkbox-child { position: absolute; margin-left: -30px; margin-top: 0; top: initial; left: initial;}

        input.permission-chck-child[type="checkbox"] {
            left: 21px;
            opacity: 0;
            position: absolute;
            z-index: 9;
            top: 0px;
        }

        input.permission-chck-child[type="checkbox"]:checked + .permission-chck-child::before {
            color: #44cc44;
            content: "";
            font-family: "FontAwesome";
            font-size: 17px;
            margin-left: 3px;
            margin-top: -7px;
            pointer-events: none;
            position: absolute;
            z-index: 1000;
            font-weight: normal;
        }

        label.permission-chck-child {
            display: inline-block;
            left: 0;
            margin-right: 28px;
            position: relative;
            top: -4px;
        }

        label.permission-accordion-checkbox-all span {
            position: relative;
            right: 25px;
            top: 2px;
        }

        label.permission-accordion-checkbox-all.select-all-chck > input.permission-chck[type="checkbox"] {
            right: 120px !important;
            top: 55px !important;
            position: relative;
        }

        label.permission-accordion-checkbox-all.select-all-chck label {
            margin-right: 0;
        }

        label.permission-accordion-checkbox-all.select-all-chck {
            float: right;
            position: relative;
            right: 54px;
            top: 20px;
        }

        label.permission-accordion-checkbox-all input.permission-chck[type="checkbox"] {
            right: 120px;
            top: 55px;
        }

        label.permission-accordion-checkbox-all .permission-chck::after {
            left: unset;
            right: 68px;
            top: -9px;
        }

        label.permission-accordion-checkbox-all input.permission-chck[type="checkbox"]:checked + .permission-chck::before {
            right: 68px;
            margin-top: -15px;
        }
        .custom-checkbox input[type="checkbox"]:not(:checked),
        .custom-checkbox input[type="checkbox"]:checked {
            position: absolute;
            left: -9999px;
        }
        .custom-checkbox input[type="checkbox"]:not(:checked) + label,
        .custom-checkbox input[type="checkbox"]:checked + label {
            cursor: pointer;
            margin: 0;
            padding-left: 27px;
            position: relative;
            color: #212121;
            font-weight: 500;
        }
        /* checkbox aspect */

        .custom-checkbox input[type="checkbox"]:not(:checked) + label:before,
        .custom-checkbox input[type="checkbox"]:checked + label:before {
            /* background: #cdcdcd; */
            content: "";
            height: 20px;
            left: 0;
            position: absolute;
            top: -5px;
            width: 20px;
            /* border: 1px solid #000; */
            background: #fff ;
            border:2px solid #8fbd3e;
        }
        /* checked mark aspect */
        div#permission-checked {
            margin-top: 40px;
        }
        .custom-checkbox input[type="checkbox"]:not(:checked) + label:after,
        .custom-checkbox input[type="checkbox"]:checked + label:after {
            background: rgba(0, 0, 0, 0) url("../images/check-icon2.png") no-repeat scroll 0 2px / 13px auto;
            color: #000000;
            content: "";
            font-size: 16px;
            height: 20px;
            left: 0.3em;
            line-height: 0.8;
            position: absolute;
            top: 4px;
            transition: all 0.2s ease 0s;
            width: 20px;
        }
        ul.right_list li {
            list-style-type: none;
            display: inline-block;
            width: 49%;
            line-height:25px;
        }

        ul.right_list {
            padding-top: 20px;
        }
        .custom-checkbox input[type="checkbox"]:not(:checked) + label:after {
            opacity: 0;
            transform: scale(0);
        }
        .custom-checkbox input[type="checkbox"]:checked + label:after {
            opacity: 1;
            transform: scale(1);
        }

        .custom-checkbox input[type="checkbox"]:disabled:not(:checked) + label:before,
        .custom-checkbox input[type="checkbox"]:disabled:checked + label:before {
            box-shadow: none;
            border-color: #bbb;
            background-color: #ddd;
        }
        .custom-checkbox input[type="checkbox"]:disabled:checked + label:after {
            color: #999;
        }
        .custom-checkbox input[type="checkbox"]:disabled + label {
            color: #aaa;
        }
        .general-detail-page.fixed-general-detail-page {
            padding-top:  25px;
        }
        div#permission-checked .custom-checkbox {
            position:  absolute;
            top: 2px;
            left: 5px;
        }
        .custom-checkbox.select-all-chck {
            text-align:  right;
        }
        .custom-checkbox.select-all-chck {
            text-align:  right;
            position:  absolute;
            top: 70px;
            right: 0px;
        }
        .custom-checkbox input[type="checkbox"]:not(:checked) + label:after, .custom-checkbox input[type="checkbox"]:checked + label:after, .custom-radio input[type="radio"]:not(:checked) + label:after, .custom-radio input[type="radio"]:checked + label:after {
            background: rgba(0, 0, 0, 0) url(/images/done.png) no-repeat scroll 3px 5px / 13px auto;
            color: #000000;
            content: "";
            font-size: 16px;
            height: 20px;
            left: 0;
            line-height: 0.8;
            position: absolute;
            top: -5px;
            transition: all 0.2s ease 0s;
            width: 20px;
            border-radius: 4px;
            background-color: #62bb31;
        }
    </style>
@stop
@section('content')
    {{--breadcrumb--}}
    @include('layouts.admin.partial.breadcrumb',['levelOne'=>'Role','levelOneLink'=>url('admin/role'),'levelTwo'=>'Competence','levelTwoLink'=>null])

    @if(Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
    @endif
    <div class="general-detail-page">
        {!! Form::open(['url'=> 'admin/role/competence', 'class' => ''] ) !!}
        {{ Form::hidden('role_id', $id) }}
        <div class="form-group clearfix">
            <!-- /.col-lg-6 (nested) -->
            <div class="col-lg-12 text-right">
                <div class="row">
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                    {!! Form::reset('Reset', ['class' => 'btn btn-sm btn-warning']) !!}
                </div>
            </div>
            <div class="form-block">
                <h2 class="modal-block-heading">Access Rights &amp; Permissions
                    >> {{app('request')->input('for')}}
                </h2>
                <div class="custom-checkbox select-all-chck">
                    <input type="checkbox" id="select-all" class="inline permission-chck" value="Select-All">
                    <label for="select-all" class="permission-accordion-checkbox-child" class=""><strong>Select All</strong></label>
                </div>
                <div class="row permission-accordion fixed-table-permission" id="permission-checked">
                    @php
                        $idIncrement = 1;
                    @endphp
                    @foreach($permissionGroup as $key => $permission)
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="access-block-inner">
                                <h3 class="access_heading btn btn-info expendCol-{{$idIncrement}}"
                                    href="#expendCol-{{$idIncrement}}"
                                    onclick="showHide('{{$idIncrement}}')">
                                    @php
                                        $idIncrementClass = strtolower(str_replace(' ','-',$key));
                                    @endphp
                                    {{trim($key)}}
                                </h3>
                                <div class="custom-checkbox">
                                    <input type="checkbox" value= "Check All" class="permission-chck" id="id{{$idIncrementClass}}"
                                           onclick="checkBelow('{{$idIncrementClass}}')" block-id="{{$idIncrementClass}}">
                                    <label for="id{{$idIncrementClass}}"></label>
                                </div>
                                <div class="access-block-body" id="expendCol-{{$idIncrement++}}">
                                    <ul class="right_list clearfix">
                                        @foreach($permission as $permissionDetails)
                                            @if(in_array($permissionDetails->name,$rolePerms))
                                                <li>
                                                    <div id="{{$permissionDetails->id}}" class="checkbox-inline">
                                                        <label class="permission-accordion-checkbox-child">
                                                            {{ Form::checkbox('permissions[]', $permissionDetails->id, true,['class'=>$idIncrementClass.' permission-chck-child permission-child-'.$idIncrementClass,'onclick'=>'toggleCheckboxChild("'.$idIncrementClass.'")']) }}
                                                            <label class="permission-chck-child"></label>
                                                        </label>
                                                        <strong data-am-popover="{content: '{{ $permissionDetails->description }}', trigger: 'hover'}">{{ ucfirst($permissionDetails->display_name) }}</strong>

                                                    </div>
                                                </li>
                                            @else
                                                <li>
                                                    <div id="{{$permissionDetails->id}}"
                                                         class="checkbox-inline">
                                                        <label class="permission-accordion-checkbox-child">
                                                            {{ Form::checkbox('permissions[]', $permissionDetails->id, null,['class'=>$idIncrementClass.' permission-chck-child permission-child-'.$idIncrementClass,'onclick'=>'toggleCheckboxChild("'.$idIncrementClass.'")']) }}
                                                            <label class="permission-chck-child"></label>
                                                        </label>
                                                        <strong data-am-popover="{content: '{{ $permissionDetails->description }}', trigger: 'hover'}">{{ ucfirst($permissionDetails->display_name) }}</strong>
                                                    </div>
                                                </li>
                                            @endif

                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        {{--<div class="col-lg-12">--}}
        {{--<div class="pull-right">--}}
        {{--<button type="submit" class="btn btn-sm btn-success">Save</button>--}}
        {{--{!! Form::reset('Reset', ['class' => 'btn btn-sm btn-warning']) !!}--}}
        {{--</div>--}}
        {{--</div>--}}
        <!-- /.col-lg-6 (nested) -->
        </div>
    {!! Form::close() !!}

    <!-- /.row (nested) -->
    </div>

@endsection
@section('js')
    <script>
      window.onload = function () {
                  {{--toggleCheckboxChild("{{$idIncrementClass}}");--}}
        var blockIds = $('input.permission-chck').map(function(){
            return $(this).attr("block-id");
          }).get();
        for(var i in blockIds)
        {
          toggleCheckboxChild(blockIds[i]);
        }
        // $(".blog-post").each(function() {
        //     $(this).append("<p>Here's a note</p>");
        // });
        // blockIds.each(function(value){
        //     toggleCheckboxChild(value);
        // });

      }
      function toggleCheckboxChild(id) {
        $('#id'+id).removeAttr('checked', 'checked');
        if(checkIfAllChildCheckboxChecked(id)){
          $('#id'+id).attr('checked', 'checked');
        }

      }

      function checkBelow(className) {
        if($('#id'+className).is(":checked")){
          $('.' + className).attr('checked', 'checked');
          $('#id'+className).val('Unchecked All');
        }else {
          $('.' + className).removeAttr('checked', 'checked')
          $('#id'+className).val('Check All');
        }

        $('#select-all').removeAttr('checked', 'checked');

        if(checkIfAllCheckboxChecked()){
          $('#select-all').attr('checked', 'checked');
        }

      }
      function checkIfAllChildCheckboxChecked(id){
        var checkedLength = $('input.permission-child-'+id+':checked').length;
        var AllLength = $('input.permission-child-'+id).length;
        if (AllLength == checkedLength) {
          return true;
        }
        return false;
      }
      function checkIfAllCheckboxChecked(){
        if ($('#permission-checked').find('input.permission-chck:checked').length == $('#permission-checked').find('input.permission-chck').length) {
          return true;
        }
        return false;
      }

      $('#select-all').click(function (event) {
        if (this.checked) {
          $('input:checkbox').attr('checked', 'checked');

        }
        else {
          $('input:checkbox').removeAttr('checked');
        }
      });


      function showHide(id) {
        $('#expendCol-' + id).toggle();
        $('#expendCol-' + id).siblings('h3').toggleClass('minus-togle-sign');
      }

    </script>

@endsection