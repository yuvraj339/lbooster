@if($selectedTheme->breadcrumb)
    <ol class="breadcrumb mt-3">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item active">{{isset($slug) ? ucwords($slug) : (isset($panelTitle) ? $panelTitle : config('app.name')) }}</li>
    </ol>
@endif