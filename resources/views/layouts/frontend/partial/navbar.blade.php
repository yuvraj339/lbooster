{{--<nav class="navbar navbar-{{$selectedTheme->navbar_style}}">
    <div class="container{{$selectedTheme->navbar_container_fluid ? '-fluid' : '' }}">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a style="color: deepskyblue;" class="navbar-brand"
               href="{{url('/')}}"><strong>{{config('app.name')}}</strong></a></div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
            <ul class="nav navbar-nav">
                --}}{{--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>--}}{{--
                @if(isset($pages))
                    @foreach($pages as $page)
                        @if($page->menu_option == 'header' || $page->menu_option == 'both')
                            <li {!! Request::is('/') ? 'class="active"' : '' !!}>
                                {!! link_to('pages/'.$page->slug, trans(ucwords($page->title))) !!}
                            </li>
                        @endif
                    @endforeach
                @endif

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">More
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        @if(Auth::guest())
                            <li>
                                <a href="{{url('register')}}">Register</a>
                            </li>
                            <li>
                                <a href="{{url('login')}}">login</a>
                            </li>
                        @else
                            <li>
                                <a href="{{url('admin')}}">Dashboard</a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out fa-fw"></i> Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        @endif

                        --}}{{--<li class="divider"></li>--}}{{--
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>--}}
<div class="container{{$selectedTheme->navbar_container_fluid ? '-fluid' : '' }}">
    <nav class="navbar navbar-expand-lg navbar-{{$selectedTheme->navbar_style}}">
        <a class="navbar-brand" href="{{url('/')}}">{{config('app.name')}}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01"
                aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
                </li>
                @if(isset($pages))
                    @foreach($pages as $page)
                        @if($page->menu_option == 'header' || $page->menu_option == 'both')
                            <li {!! Request::is('/') ? 'class="nav-item active"' : 'class="nav-item"' !!}>
                                <a class="nav-link" href="{{url('pages/'.$page->slug)}}">
                                    {{-- {!! link_to() !!}--}}
                                    {{ ucwords($page->title) }}
                                </a>
                            </li>
                        @endif
                    @endforeach
                @endif
            </ul>
            <ul class="nav navbar-nav ml-auto">
                {{--<li class="nav-item">
                    <a class="nav-link active" href="#">Active</a>
                </li>--}}
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download"
                       aria-expanded="false">More<span class="caret"></span></a>
                    <div class="dropdown-menu" aria-labelledby="download">
                        @if(Auth::guest())
                            <a class="dropdown-item" href="{{url('register')}}">Register</a>
                            <a class="dropdown-item" href="{{url('login')}}">login</a>
                        @else
                            <a class="dropdown-item" href="{{url('admin')}}">Dashboard</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out fa-fw"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                  style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endif
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>