@extends('layouts.frontend.layout')
@section('title') {!! isset($panelTitle) ? $panelTitle : config('app.name') !!}
@stop
@section('content')
    <div class="col-md-6 offset-md-3">
        @if (isset($panelBody))
            <div class="panel-body">
                <div class="">
                    {{ $panelBody }}
                </div>
            </div>
        @endif

    </div>
@stop
